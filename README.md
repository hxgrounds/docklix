# Docklix

This is a command line tool, created to allow easily running [lix]¹ commands inside docker.

> 1 - Lix is a package manager for [haxe] language.

With docklix you can ensure that your build runs in a consistent environment.
Also, you may not need to install some dependencies in your enviroment.

[lix]: https://github.com/lix-pm/lix.client
[haxe]: https://haxe.org/

## Roadmap

* [x] Running lix command from docker at current directory
* [x] Use current uid and gid
* [x] Support run on user defined docker images
* [x] Allows to select default images based on supported targets
* [x] Installation with `npm` or `yarnpkg` to allow running without lix or haxelib
* [ ] Run command for multiple images and/or targets

See [hxgrounds/haxe-images](https://gitlab.com/hxgrounds/haxe-images) on gitlab
or [hxgrounds user](https://hub.docker.com/u/hxgrounds) on docker hub
for the docker images we are developing.

Currently created docker images with lix:

* [x] neko
* [x] nodejs
* [x] c++
* [x] c#
* [x] java
* [x] python
* [x] lua
* [x] php
* [x] haxelink
* [ ] maybe: swf, as3, js (browser)

## How to Use

### Install

The tool can be installed using haxelib, lix or npm:

* haxelib:
    - `haxelib install docklix`
* lix:
    - `lix install haxelib:docklix`
* npm:
    - `npm install --global docklix`

### Execute

Run docklix similar to how you would use lix.

For example, instead of: `lix run munit test -neko`

you could use: `docklix run munit test -neko`

> **NOTE:** If installed with lix or haxelib, prefix with related commands.
> Example: `lix run docklix ...` ou `haxelib run docklix ...`.

If needed, you can use `--` to separate args passed to lix (inside the container)
and the options provided to docklix itself.

For example:

`haxelib run docklix -- --help`

#### Select image

Docklix supports some haxe targets by default (as listed above).

These targets can be used to select one of the predefined images.
For example:

`docklix -t python run ...`

To see current supported targets run: `docklix --help`

Alternatively, a custom image can be selected with:

`docklix -i <image> ...`

--------------

## Contributors

### How to compile and test

This project uses the [lix package manager](https://github.com/lix-pm/lix.client).
So, to install its dependencies run:

`lix download`

For nodejs builds, there are dependencies that need to be downloaded.
So run:

`yarnpkg` or `npm install`

To run the tests, you can use:

`lix run munit test -<target>`

For example:

`lix run munit test -neko`

`lix run munit test -js`


### Supported systems

Currently testing on `neko`, `nodejs` and `python`, but should work with other `sys` languages.

> NOTE: tests are failing in java due to an error in haxe hamcrest

--------------

## License

This project is licensed under the MIT [non-AI license](https://github.com/non-ai-licenses/non-ai-licenses). See [LICENSE.md](./LICENSE.md) file.

So the content is this repository is not to be used on AI training datasets or technologies.