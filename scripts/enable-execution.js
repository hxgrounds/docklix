const process = require('process');
const fs = require('fs');

async function turnIntoExecutable(filePath){
    const st = await fs.promises.stat(filePath);
    const newmode = st.mode |
                    fs.constants.S_IXUSR |
                    fs.constants.S_IXGRP |
                    fs.constants.S_IXOTH;

    return await fs.promises.chmod(filePath, newmode);
}

async function main(){
    var files = process.argv.slice(2);

    await Promise.all(files.map(async (file) => {
        await turnIntoExecutable(file);
    }));
}

main();