#!/bin/sh

# build --------------------------
haxe build.neko.hxml

# package ------------------------
mkdir -p build
rm -f build/library.zip
zip -r build/library.zip src *.md *.json run.n

# submit -------------------------
# Password should be stored as base64
export DECODED_PASS=$(echo $HAXELIB_PASS | base64 --decode)

haxelib submit build/library.zip "$DECODED_PASS" --always