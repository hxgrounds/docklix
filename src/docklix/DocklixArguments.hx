package docklix;

@:structInit
class DocklixArguments {

    @:optional
    public var lixArgs : Array<String>;

    @:optional
    public var docklixArgs: Array<String>;
}