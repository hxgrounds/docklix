package docklix;


import tink.Cli;
import tink.cli.Rest;

import haxe.io.Path;

import proxsys.System;


class DocklixCli{

    /** Specify in order to print docklix help. **/
    public var help:Bool = false;

    /** Adds a volume to the docker container. May be specified multiple times.
        Uses the docker volumes syntax: <host path>:<container path>
        For example:
            docklix -v $PWD/../mypath:/example run example-lib

        NOTE: by default the host haxe_libraries and current path (PWD) are already
            mounted inside the container.
    **/
    @:alias("v")
    @:flag("volume")
    public var volumes:Array<String> = null;

    /** User ID or username with (optionally) group ID/name (format: <name|uid>[:<group|gid>]) **/
    @:alias("u")
    @:flag("user")
    public var uid_gid:String = null;

    /** Docker image used to run the lix command.
        NOTE: the image should have a lix executable. **/
    @:alias("i")
    public var image:String = null;

    /**
        Select docker image based on one of the supported targets.
    **/
    @:alias("t")
    public var target:String = null;

    var lixArgs(default, null):Array<String>;

    var sys:System;
    var cmd:DocklixCommand;

    public function new(sys:System, ?cmd:DocklixCommand){
        this.sys = sys;
        this.cmd = cmd;
    }

    public function withLixArgs(args:Array<String>) {
        this.lixArgs = args;
        return this;
    }

    @:defaultCommand
    public function docklix(args:Rest<String>) {
        if(this.help || hasNoArgs(args)){
            showHelp();
            return;
        }

        if(args.length == 0 && this.lixArgs != null){
            args = this.lixArgs;
        }

        this.lix(args);
    }

    function hasNoArgs(args:Array<String>) {
        var argsEmpty = (args == null || args.length == 0);
        var lixArgsEmpty = (lixArgs == null || lixArgs.length == 0);

        return argsEmpty && lixArgsEmpty;
    }

    public function lix(args:Rest<String>) {
        this.cmd.execute(args, {
            volumes: this.volumes,
            user_and_gid: this.uid_gid,
            image: this.image,
            target: this.target
        });
    }

    // ----------------------------

    function showHelp() {
        var targets = cmd.supportedTargets().join(", ");

        sys.println(usageHeader() + Cli.getDoc(this) + '

  Supported targets: ${targets}
');
    }

    function usageHeader(): String{
        var progName = programName();

        return
'Usage: $progName [<docklix-options>] [--] [<lix-commands>]

Docklix options should be specified before the first lix command.
In case no lix command is specified, use \'--\' to separate lix options.

For example, to see lix help you can use:
        $progName -- --help

Or simple:
        $progName

Below are listed current docklix options.
';
    }

    function programName(): String {
        var programFilename = Path.withoutDirectory(Sys.programPath());

        return programFilename;
    }
}