package docklix;

import docklix.docker.ImageSelector;
import docklix.docker.DockerCommand;
import proxsys.System;

typedef DocklixOptions = {
    ?volumes: Array<String>,
    ?user_and_gid: String,
    ?image: String,
    ?target: String
};

class DocklixCommand {
    public static final CONTAINER_HOME = '/home/lix-user';
    static final DEFAULT_IMAGE = "hxgrounds/lix-haxe";

    var sys:System;
    var userGetter:UserGetter;
    var imageSelector:ImageSelector;


    public function new(sys:System, userGetter:UserGetter) {
        this.sys = sys;
        this.userGetter = userGetter;
        this.imageSelector = new ImageSelector();
    }

    public function execute(args: Array<String>, ?options: DocklixOptions) {
        if(options == null) options = {};

        var commandArgs = ['lix'].concat(args);

        var pwd = sys.getEnv('PWD');
        var home = sys.getEnv('HOME');

        var other_volumes = if(options.volumes != null) options.volumes else [];

        var cmd = DockerCommand.init();

        if(options.user_and_gid == null){
            options.user_and_gid = this.fetchUserAndGid();
        }

        if(options.user_and_gid != null){
            cmd.user_and_gid(options.user_and_gid);

            //Keep default container home
            cmd.env('HOME', CONTAINER_HOME);
        }

        var image = imageSelector.selectImage(options.image, options.target);

        cmd.iteractive()
            .temporary()
            .volume('$home/haxe/haxe_libraries', '$CONTAINER_HOME/haxe/haxe_libraries')
            .volume(pwd, pwd)
            .raw_volumes(...other_volumes)
            .workdir(pwd)
            .run(image, ...commandArgs)
            .execute(sys);
    }

    function fetchUserAndGid(): String {
        var userInfo = this.userGetter.fetchUserInfo();

        if(userInfo != null){
            return userInfo.formatUserAndGid();
        }

        return null;
    }

    public function supportedTargets() {
        return this.imageSelector.supportedTargets();
    }
}