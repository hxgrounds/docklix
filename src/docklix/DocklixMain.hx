package docklix;

import docklix.sys.SystemUserGetter;
import docklix.cli.DocklixCliRunner;
import proxsys.System;
import proxsys.StdSystem;


class DocklixMain {
    static function main() {
        new DocklixMain().run();
    }

    var sys:System;
    var cmd:DocklixCommand;
    var cli:DocklixCli;

    var cliRunner:DocklixCliRunner;

    public function new() {
        sys = new StdSystem();
        cmd = new DocklixCommand(sys, new SystemUserGetter(sys));
        cli = new DocklixCli(sys, cmd);

        cliRunner = new DocklixCliRunner();
    }

    public function run() {
        cliRunner.run(cli, sys.args());
    }
}