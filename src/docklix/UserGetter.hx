package docklix;

import docklix.domain.UserInfo;

interface UserGetter {
    /** Retrieve user info.
        May return null if unsupported or unavailable.
    **/
    function fetchUserInfo(): Null<UserInfo>;
}