package docklix.cli;

import haxe.Rest;

class DocklixCliParser {
    static final SEPARATOR = '--';

    var optionsWithValues:Map<String, Bool>;

    public function new() {
        optionsWithValues = [];
    }

    public function parse(args:Array<String>): DocklixArguments{
        var splitIdx = findSplitPoint(args);
        var lStart   = if(isSeparator(args, splitIdx)) splitIdx + 1 else splitIdx;

        return {
            docklixArgs: args.slice(0,      splitIdx),
            lixArgs    : args.slice(lStart, args.length)
        };
    }

    function findSplitPoint(arr:Array<String>) {
        var i = 0;
        while (i < arr.length){
            if(!isDocklixOption(arr[i])){
                return i;
            }
            else if(isOptionWithValue(arr[i])){
                //skips the next argument, because it should be the option value
                ++i;
            }

            ++i;
        }
        return arr.length;
    }

    function isOptionWithValue(option:String) {
        return optionsWithValues.exists(option);
    }

    function isSeparator(arr:Array<String>, idx:Int) {
        return idx < arr.length && arr[idx] == SEPARATOR;
    }

    function isDocklixOption(arg: String) {
        return isOption(arg) && arg != SEPARATOR;
    }

    function isOption(a:String) {
        return a.length > 1 && a.charAt(0) == '-';
    }

    public function addOptionsWithValues(options:Rest<String>) {
        for(opt in options){
            optionsWithValues.set(opt, true);
        }
    }
}