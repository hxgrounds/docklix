package docklix.cli;

import haxe.Exception;
import tink.core.Error;
import tink.core.Noise;
import tink.core.Outcome;
import tink.core.Callback;
import tink.Cli;

typedef RunHandle = (Null<Exception>)->Void;

class DocklixCliRunner {
    static final OPTIONS_WITH_VALUES = [
        '-v', '--volume', '-u', '--user',
        '-i', '--image',
        '-t', '--target'
    ];

    var parser:DocklixCliParser;

    public var runHandle(null, default):Callback<Outcome<Noise, Error>>;

    public function new(?parser: DocklixCliParser, ?runHandle:RunHandle) {
        this.parser = if(parser != null) parser else new DocklixCliParser();
        this.runHandle = if(runHandle == null) Cli.exit else (outcome)->{
            runHandle(exceptionFromOutcome(outcome));
        };

        this.parser.addOptionsWithValues(...OPTIONS_WITH_VALUES);
    }

    static function exceptionFromOutcome(outcome: Outcome<Noise, Error>): Null<Exception> {
        switch (outcome){
            case Failure(failure): {
                try {
                    failure.throwSelf();
                }
                catch(e){
                    return e;
                }
            }
            case Success(_): return null;
        };

        return null;
    }

    public function run(
        docklixCli:DocklixCli, args:Array<String>)
    {
        var parsed = parser.parse(args);
        var cli = docklixCli.withLixArgs(parsed.lixArgs);

        Cli.process(parsed.docklixArgs, cli).handle(runHandle);
    }
}