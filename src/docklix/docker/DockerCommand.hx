package docklix.docker;

import proxsys.System;


class DockerCommand {
    public static function init() {
        return new DockerCommand();
    }

    var cmd:String;
    var cmdOptions:Array<String>;
    var cmdArgs:Array<String>;

    public function new() {
        cmdOptions = [];
        cmdArgs = [];
    }

    public function env(variable:String, value:String) {
        return addCommandOptions('-e').addCommandOptions('$variable=$value');
    }

    public function it()         { return iteractive();}
    public function iteractive() { return addCommandOptions("-it"); }

    public function rm()        { return temporary();}
    public function temporary() { return addCommandOptions("--rm");};

    public function v     (host_p:String, container_p:String) { return volume(host_p, container_p);}
    public function volume(host_path:String, container_path:String) {
        return raw_volumes('${host_path}:${container_path}');
    };

    public function raw_volumes(...raw_volumes:String) {
        for(v in raw_volumes){
            addCommandOptions('-v').addCommandOptions(v);
        }
        return this;
    }

    public function user_and_gid(uid_and_gid: String) {
        return addCommandOptions('-u').addCommandOptions(uid_and_gid);
    }

    public function w      (container_path:String) { return workdir(container_path);}
    public function workdir(container_path:String) {
        return addCommandOptions('-w').addCommandOptions(container_path);
    };

    public function addCommandOptions(opt:String) {
        this.cmdOptions.push(opt);
        return this;
    }

    public function run(img:String, ...cmdArgs:String ) {
        cmd = "run";
        this.cmdArgs = [img].concat(cmdArgs);

        return this;
    }

    public function program(){
        return "docker";
    }

    public function commandArgs(){
        var args = [];

        if(cmd != null){
            args.push(cmd);

            if(cmdOptions.length > 0){
                args = args.concat(cmdOptions);
            }

            if(this.cmdArgs != null){
                args = args.concat(cmdArgs);
            }
        }
        return args;
    }

    public function execute(sys: System) {
        sys.command(program(), commandArgs());
    }
}