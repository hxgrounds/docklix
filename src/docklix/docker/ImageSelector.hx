package docklix.docker;

import haxe.Exception;

class UnsupportedTargetException extends Exception {}
class IllegalSelectionParameters extends Exception {}

class ImageSelector {
    static final IMAGE_ORG = "hxgrounds/";
    static final DEFAULT_IMAGE_NAME = "lix-haxe";

    public static final DEFAULT_IMAGE = IMAGE_ORG + DEFAULT_IMAGE_NAME;

    public static final TARGET_TO_IMAGE = [
        'neko'     => DEFAULT_IMAGE_NAME,
        'nodejs'   => "haxe-nodejs",
        'java'     => "haxe-java",
        'python'   => "haxe-python",
        'c++'      => "haxe-cpp",
        'cpp'      => "haxe-cpp",
        'hl'       => "haxe-hashlink",
        'hashlink' => "haxe-hashlink",
        'lua'      => "haxe-lua",
        "csharp"   => 'haxe-csharp:mono',
        "mono"     => 'haxe-csharp:mono',
        "dotnet"   => 'haxe-csharp:dotnet'
    ];

    public function new() {
    }

    public function selectImage(image:String, ?target:String) {
        if(target != null && !TARGET_TO_IMAGE.exists(target)){
            throw new UnsupportedTargetException('Target {$target} is not supported.');
        }
        if(image != null && target != null){
            throw new IllegalSelectionParameters(
                'Trying to select by both image ($image) and target ($target)');
        }

        if(image != null){
            return image;
        }
        if(target != null){
            return IMAGE_ORG + TARGET_TO_IMAGE.get(target);
        }

        return DEFAULT_IMAGE;
    }

    public function supportedTargets() {
        return [for (t in TARGET_TO_IMAGE.keys()) t];
    }
}