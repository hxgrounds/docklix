package docklix.domain;

@:structInit
class UserInfo{
    public var uid(default, default): Null<Int>;
    public var gid(default, default): Null<Int>;

    public function new(?uid: Int, ?gid: Int) {
        this.uid = uid;
        this.gid = gid;
    }

    public function formatUserAndGid() {
        if(uid != null){
            return '$uid'
                + if(gid != null) ':$gid' else '';
        }
        return null;
    }
}