package docklix.sys;

import docklix.domain.UserInfo;
import proxsys.System;

using proxsys.tools.ProcessTools;


class SystemUserGetter implements UserGetter{
    var system:System;

    public function new(system: System) {
        this.system = system;
    }

    public function fetchUserInfo():Null<UserInfo> {
        var pUid = this.system.startProcess('id', ['-u']);
        var pGid = this.system.startProcess('id', ['-g']);

        var uidStr = pUid.readAllText();
        var gidStr = pGid.readAllText();

        return {
            uid: Std.parseInt(uidStr),
            gid: Std.parseInt(gidStr)
        };
    }
}