import massive.munit.TestSuite;

import docklix.test.DocklixCommandTest;
import docklix.test.domain.UserInfoTest;
import docklix.test.cli.DocklixCliParserTest;
import docklix.test.cli.DocklixCliRunnerTest;
import docklix.test.DocklixCliTest;
import docklix.test.sys.SystemUserGetterTest;
import docklix.test.docker.DockerCommandTest;
import docklix.test.docker.ImageSelectorTest;

/**
 * Auto generated Test Suite for MassiveUnit.
 * Refer to munit command line tool for more information (haxelib run munit)
 */
class TestSuite extends massive.munit.TestSuite
{
	public function new()
	{
		super();

		add(docklix.test.DocklixCommandTest);
		add(docklix.test.domain.UserInfoTest);
		add(docklix.test.cli.DocklixCliParserTest);
		add(docklix.test.cli.DocklixCliRunnerTest);
		add(docklix.test.DocklixCliTest);
		add(docklix.test.sys.SystemUserGetterTest);
		add(docklix.test.docker.DockerCommandTest);
		add(docklix.test.docker.ImageSelectorTest);
	}
}
