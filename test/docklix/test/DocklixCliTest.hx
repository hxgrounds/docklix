package docklix.test;

import docklix.cli.DocklixCliParser;
import docklix.DocklixCommand.DocklixOptions;
import massive.munit.Assert;
import proxsys.FakeSystem;
import docklix.cli.DocklixCliRunner;
import docklix.test.fakes.DocklixCommandFake;

import org.hamcrest.Matchers;
import org.hamcrest.Matchers.*;

class DocklixCliTest {
    var cli:DocklixCli;

    var fakeSys  : FakeSystem;
    var fakeCmd  : DocklixCommandFake;
    var cliRunner:DocklixCliRunner;

    public function new() {}

    @Before
    public function setup() {
        fakeCmd = new DocklixCommandFake();
        fakeSys = new FakeSystem();
        cli     = new DocklixCli(fakeSys, fakeCmd);

        var handler:RunHandle = (outcome)->{};
        cliRunner = new DocklixCliRunner(handler);
    }

    @Test
    public function test_run_lib() {
        // Given: these arguments
        var lixArgs = ["run", "lib"];

        //When executing:
        runLix(lixArgs);

        fakeCmd.verifyLastCommandExecutedWith(lixArgs);
    }

    @Test
    public function test_run_lix_with_options() {
        // Given: this argument
        var lixArgs = ["--version"];
        var args = ["--"].concat(lixArgs);

        //When executing:
        runLix(args);

        fakeCmd.verifyLastCommandExecutedWith(lixArgs);
    }


    @Test
    public function test_run_lix_with_command_and_options() {
        // Given: this argument
        var lixArgs = ["run", "lib", "--example", "value"];

        //When executing:
        runLix(lixArgs);

        fakeCmd.verifyLastCommandExecutedWith(lixArgs);
    }

    @Test
    public function test_run_help() {
        // Given: this argument
        var args = ["--help"];

        //When executing:
        runLix(args);

        //Then: Should not execute docker commands
        fakeCmd.verifyNoCommandExecuted();

        //And: program usage should be printed
        Assert.isNotEmpty(fakeSys.printedText(), "Should have printed something");
    }

    @Test
    public function test_run_help_when_no_argument_is_provided() {
        //When executing:
        runLix([]);

        //Then: Should not execute docker commands
        fakeCmd.verifyNoCommandExecuted();

        //And: program usage should be printed
        Assert.isNotEmpty(fakeSys.printedText(), "Should have printed something");
    }

    @Test
    public function test_add_docker_volumes() {
        // Given: this argument
        var volumes = ["host/path:/container/path", "/other/path"];
        var args = ["-v", volumes[0], '-v', volumes[1], "run", "lib"];

        //When executing:
        runLix(args);

        //Then:
        assertThat(cli.volumes, is(both(notNullValue()).and(equalTo(volumes))),
            "Should have parsed the specified volumes");

        assertThat(fakeCmd.lastCommandOptions(), is(notNullValue()));

        assertThat(
            fakeCmd.lastCommandOptions().volumes, equalTo(volumes),
            "Docklix command should receive specified volumes"
        );
    }

    @Test
    public function test_set_uid_and_gid() {
        // Given: these arguments
        var uid = 1000;
        var gid = 1001;
        var uid_gid = '$uid:$gid';
        var args = ["-u", uid_gid, "run", "lib"];

        //When executing:
        runLix(args);

        //Then:
        var opt = fakeCmd.lastCommandOptions();

        assertThat(opt, is(notNullValue()), "Should have executed command");

        Matchers.assertThat(opt.user_and_gid, Matchers.equalTo(uid_gid),
            "Command user id and gid does not match the expected value");
    }

    @Test
    public function set_user_image() {
        var dockerImage = "example";
        var args = ["--image", dockerImage, "--", "--help"];

        runLix(args);

        var opt = requireLastCommand();

        assertThat(opt.image, is(equalTo(dockerImage)),
            "Docker image does not match the expected value");
    }

    @Test
    public function set_target_for_docklix_command() {
        var target = 'neko';

        runLix(["-t", target, "--", "run", "lib"]);

        var opt = requireLastCommand();

        assertThat(opt.target, is(equalTo(target)),
            'Expected target does not match'
        );
    }

    function requireLastCommand(): DocklixOptions {
        var opt = fakeCmd.lastCommandOptions();

        assertThat(opt, is(notNullValue()), "Should have executed a command");

        return opt;
    }

    // Helpers ----------------------------------------------

    function runLix(args: Array<String>) {
        cliRunner.run(cli, args);
    }
}