package docklix.test;

import docklix.docker.ImageSelector;
import docklix.test.fakes.FakeUserGetter;
import docklix.DocklixCommand.DocklixOptions;
import massive.munit.Assert;

import proxsys.FakeSystem;

import org.hamcrest.Matchers;
import org.hamcrest.Matchers.*;


class DocklixCommandTest {
    var cmd:DocklixCommand;
    var sys:FakeSystem;
    var fakeUserGetter:FakeUserGetter;

    static final PWD = '/my/dir';
    static final HOME = '/home/anybody';
    static final CONTAINER_HOME = DocklixCommand.CONTAINER_HOME;

    public function new() {
    }

    @Before
    public function setup() {
        sys = new FakeSystem();
        fakeUserGetter = new FakeUserGetter();

        cmd = new DocklixCommand(sys, fakeUserGetter);

        sys.putEnv('PWD', PWD);
        sys.putEnv('HOME', HOME);
    }

    @Test
    public function test_run_lib() {
        //When executing:
        runLix(["run", "lib"]);

        //Then: docker should be executed with these parameters
        Assert.isNotNull(sys.lastCommand(), "should have executed one command");
        Assert.areEqual(
            "docker run -it --rm"
            + ' -v ${HOME}/haxe/haxe_libraries:$CONTAINER_HOME/haxe/haxe_libraries'
            + ' -v $PWD:$PWD -w $PWD'
            + ' hxgrounds/lix-haxe'
            + ' lix run lib',
            sys.lastCommand()
        );
    }


    @Test
    public function test_add_volumes() {
        // Given: this directories
        var volumes = ['/path1:/root/p1', '/mnt/example'];

        //When executing:
        runLix(["run", "lib"], {
            volumes: volumes
        });

        //Then: docker command should contain volumes
        Matchers.assertThat(sys.lastCommand(),
            Matchers.containsString('-v ' + volumes.join(' -v ')),
            "Docker command should contain specified volumes"
            );
    }

    @Test
    public function by_default_command_should_use_current_uid_and_gid_on_container() {
        // Given:
        fakeUserGetter.uid = 1234;
        fakeUserGetter.gid = 4321;

        // When:
        runLix(['run', 'lib']);

        //Then:
        command_should_contain_user_gid(
            '${fakeUserGetter.uid}:${fakeUserGetter.gid}');
    }

    @Test
    public function test_set_userid() {
        // Given:
        var uid = '1000';

        //When executing:
        runLix(["run", "lib"], {
            user_and_gid: uid
        });

        //Then: docker command should contain volumes
        Matchers.assertThat(sys.lastCommand(),
            Matchers.containsString('-u ' + uid),
            "Docker command should contain specified user id"
        );

        //And:
        this.command_should_keep_default_container_home();
        this.command_should_not_fetch_user_id();
    }


    @Test
    public function test_set_userid_and_group_id() {
        // Given:
        var user = '1000:1001';

        //When executing:
        runLix(["run", "lib"], {
            user_and_gid: user
        });

        //Then: docker command should contain volumes
        command_should_contain_user_gid(user);

        //And:
        this.command_should_keep_default_container_home();
        this.command_should_not_fetch_user_id();
    }

    @Test
    public function set_container_image() {
        //Given
        var image = "org/image";

        //When
        runLix(["run", "lib"], {
            image: image
        });

        //Then
        command_should_contain('$image lix run lib',
            "Command should contain specified image");
    }

    @Test
    public function select_default_image_for_target() {
        // var m = ['neko' => 'lix-haxe'];

        for (target => image in ImageSelector.TARGET_TO_IMAGE){
            test_select_target(target, image);
        }
    }

    inline function test_select_target(target:String, expectedImage: String) {
        setup();
        runLix(["run", "lib"], {
            target: target
        });


        command_should_contain('$expectedImage lix',
            'Command should contain expected image: "$expectedImage"');
    }

    // Helpers -------------------------------------------------------------

    function runLix(args: Array<String>, ?options: DocklixOptions) {
	    cmd.execute(args, options);
    }

    inline function command_should_contain_user_gid(userGid: String) {
        command_should_contain('-u $userGid',
            "Docker command should contain the composition of user id and group id"
        );
    }

    function command_should_not_fetch_user_id() {
        assertThat(fakeUserGetter.fetchCallsCount, is(0),
            'Should not have fetch user info');
    }

    inline function command_should_keep_default_container_home() {
        command_should_contain('-e HOME=$CONTAINER_HOME',
            'Docker command should keep default container home');
    }

    function command_should_contain(text:String, errMsg:String) {
        assertThat(sys.lastCommand(), containsString(text), errMsg);
    }
}