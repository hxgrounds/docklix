package docklix.test.cli;

import massive.munit.Assert;
import docklix.cli.DocklixCliParser;


class DocklixCliParserTest {

    var parser:DocklixCliParser;

    public function new() {

    }

    @Before
    public function setup() {
        parser = new DocklixCliParser();
    }

    @Test
    public function test_parse_empty_args() {
        checkParsing([], [], []);
    }

    @Test
    public function test_parse_simple_args() {
        // Given: these arguments
        var lixArgs = ["run", "lib"];

        //When parsing those args
        checkParsing(lixArgs, lixArgs, []);
    }

    @Test
    public function test_parse_with_docker_options() {
        // Given: these arguments
        var lixArgs = ["run", "lib", "--option"];

        //When parsing those args
        checkParsing(lixArgs, lixArgs, []);
    }

    @Test
    public function test_parse_dockerlix_options() {
        // Given: these arguments
        var docklixArgs = ["--js"];
        var lixArgs = ["run", "lib", "--option"];

        //When parsing those args
        checkParsing(docklixArgs.concat(lixArgs), lixArgs, docklixArgs);
    }

    @Test
    public function test_parse_only_docklix_args() {
        var dArgs = ["--opt1", "--opt2"];

        checkParsing(dArgs, [], dArgs);
    }

    @Test
    public function test_parse_using_explicit_separator() {
        // Given: these arguments
        var docklixArgs = ["--js"];
        var lixArgs     = ["--help"];
        var args = docklixArgs.concat(["--"]).concat(lixArgs);

        //When parsing those args
        checkParsing(args, lixArgs, docklixArgs);
    }

    @Test
    public function test_parse_argument_with_value() {
        // Given: these options with values
        parser.addOptionsWithValues('-v', '--volume');

        // Given: these arguments
        var docklixArgs = ['-v', '/path:/root/path', '--volume', 'example', '--option'];
        var lixArgs     = ['run', 'lib'];

        var args = docklixArgs.concat(lixArgs);

        //When parsing those args
        checkParsing(args, lixArgs, docklixArgs);
    }

    function checkParsing(
        args:Array<String>, lixArgs:Array<String>, docklixArgs:Array<String>)
    {
        var parsed = doParse(args);

        Assert.areEqual(lixArgs, parsed.lixArgs);
        Assert.areEqual(docklixArgs, parsed.docklixArgs);
    }


    function doParse(args: Array<String>) {
        return parser.parse(args);
    }
}