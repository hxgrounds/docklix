package docklix.test.cli;

import haxe.Exception;
import proxsys.FakeSystem;
import massive.munit.Assert;
import docklix.test.fakes.FakeDocklixCli;
import docklix.test.fakes.DocklixCommandFake;
import docklix.cli.DocklixCliRunner;


class DocklixCliRunnerTest {

    var fakeCommand:DocklixCommandFake;

    var cliRunner:DocklixCliRunner;
    var fakeCli:FakeDocklixCli;
    var docklixCli:DocklixCli;

    public function new() {

    }

    @Before
    public function setup() {
        cliRunner = new DocklixCliRunner(ignoreOutcome());

        fakeCommand = new DocklixCommandFake();
        docklixCli  = new DocklixCli(new FakeSystem(), fakeCommand);
        fakeCli = new FakeDocklixCli();
    }

    function ignoreOutcome():RunHandle {
        return (outcome)->{};
    }

    @Test
    public function test_pass_lix_args() {
        var lixArgs = ["run", "example", "--option"];

        //when:
        cliRunner.run(fakeCli, lixArgs);

        //then:
        fakeCli.lixArgsShouldBe(lixArgs);
    }


    @Test
    public function test_execute_help() {
        //given:
        var args = ["--help"];

        //when:
        cliRunner.run(fakeCli, args);

        //then:
        Assert.isTrue(fakeCli.help, "Help option should be true");

        fakeCli.lixArgsShouldBe([]);
    }

    @Test
    public function test_options_with_values() {
        //given:
        var dArgs = ['-v', 'volume:/value', '--volume', '/volumes/full'];
        var lixArgs = ['run', 'lib'];
        var args = dArgs.concat(lixArgs);

        //when:
        cliRunner.run(fakeCli, args);

        //then:
        fakeCli.lixArgsShouldBe(lixArgs);
    }
}