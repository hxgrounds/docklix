package docklix.test.docker;

import massive.munit.Assert;

import proxsys.FakeSystem;

import docklix.docker.DockerCommand;

class DockerCommandTest {
    var sys:FakeSystem;

    public function new() {

    }

    @Before
    public function setup() {
        sys = new FakeSystem();
    }

    @Test
    public function command_args() {
        var cmd = DockerCommand.init().run("image");

        Assert.areEqual("docker", cmd.program());
        Assert.areEqual(["run", "image"], cmd.commandArgs());
    }

    @Test
    public function run_image() {
        checkCommand("docker run image", DockerCommand.init().run("image"));
    }

    @Test
    public function run_image_command() {
        checkCommand("docker run image sh", DockerCommand.init().run("image", "sh"));
    }


    @Test
    public function run_image_iterative() {
        var expected = "docker run -it image sh";

        checkCommand(expected, DockerCommand.init().run("image", "sh").iteractive());
        checkCommand(expected, DockerCommand.init().run("image", "sh").it());
    }

    @Test
    public function run_temp_container() {
        var expected = "docker run --rm image";

        checkCommand(expected, DockerCommand.init().run("image").temporary());
        checkCommand(expected, DockerCommand.init().run("image").rm());
    }


    @Test
    public function run_with_volume() {
        var host_path = "my/host/path";
        var container_path = "/container/path";
        var expected = 'docker run -v ${host_path}:${container_path} image';

        checkCommand(expected, DockerCommand.init().run("image").volume(host_path, container_path));
        checkCommand(expected, DockerCommand.init().run("image").v(host_path, container_path));
    }

    @Test
    public function add_raw_volumes() {
        var raw_volumes = ["/vol1", "/v2:/root/v2"];

        var volumes_str = raw_volumes.join(' -v ');
        var expected = 'docker run -v $volumes_str image';

        checkCommand(expected, DockerCommand.init().run("image").raw_volumes(...raw_volumes));
    }


    @Test
    public function run_at_workdir() {
        var workdir = "/root/my/container/workdir";
        var expected = 'docker run -w ${workdir} image';

        checkCommand(expected, DockerCommand.init().run("image").workdir(workdir));
        checkCommand(expected, DockerCommand.init().run("image").w(workdir));
    }

    @Test
    public function add_env_variable() {
        var expected = 'docker run -e a=1 -e b=2 image';
        var command  = DockerCommand.init().run("image").env('a', '1').env('b', '2');

        checkCommand(expected, command);
    }

    // ------------------------------------------------------------------

    function checkCommand(expected:String, cmd:DockerCommand) {
        // sys.command(cmd.program(), cmd.commandArgs());
        cmd.execute(sys);

        lastCommandShouldBe(expected);
    }

    function lastCommandShouldBe(expected: String){
        Assert.isNotNull(sys.lastCommand(), "No command produced");
        Assert.areEqual(expected, sys.lastCommand(),
            'Expected command: "${expected}". Found: "${sys.lastCommand()}"');
    }
}