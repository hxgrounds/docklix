package docklix.test.docker;

import massive.munit.Assert;
import org.hamcrest.Matchers;
import haxe.Exception;
import docklix.docker.ImageSelector;

import org.hamcrest.Matchers.*;


class ImageSelectorTest {
    var imageSelector:ImageSelector;

    public function new() {
    }

    @Before
    public function setup() {
        imageSelector = new ImageSelector();
    }

    @Test
    public function select_default_image() {
        test_select_image(null, null, ImageSelector.DEFAULT_IMAGE);
    }

    @Test
    public function select_specified_image() {
        var img = "myimage";

        test_select_image(img, null, img);
    }

    @Test
    public function select_image_by_target() {
        var org = "hxgrounds";

        test_select_image(null, "neko",     '$org/lix-haxe');
        test_select_image(null, "nodejs",   '$org/haxe-nodejs');
        test_select_image(null, "java",     '$org/haxe-java');
        test_select_image(null, "python",   '$org/haxe-python');
        test_select_image(null, "c++",      '$org/haxe-cpp');
        test_select_image(null, "cpp",      '$org/haxe-cpp');
        test_select_image(null, "hl",       '$org/haxe-hashlink');
        test_select_image(null, "hashlink", '$org/haxe-hashlink');
        test_select_image(null, "lua",      '$org/haxe-lua');
        test_select_image(null, "csharp",   '$org/haxe-csharp:mono');
        test_select_image(null, "mono",     '$org/haxe-csharp:mono');
        test_select_image(null, "dotnet",   '$org/haxe-csharp:dotnet');
    }

    @Test
    public function try_to_select_unsupported_target() {
        select_image_should_throw(null, "invalid-target", UnsupportedTargetException);
    }

    @Test
    public function try_to_select_image_and_target() {
        select_image_should_throw("image", "neko", IllegalSelectionParameters);
    }

    function select_image_should_throw(image:String, target:String, exceptionClass:Class<Exception>) {
        Assert.throws(exceptionClass, ()->{
            imageSelector.selectImage(image, target);
        });
    }

    // --------------------------------------------------------------------

    function test_select_image(image:String, target:String, expectedImage:String) {
        assertThat(imageSelector.selectImage(image, target), equalTo(expectedImage),
            'Selected image for target {$target} should be: "${expectedImage}"'
        );
    }
}