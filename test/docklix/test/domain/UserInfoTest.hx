package docklix.test.domain;

import docklix.domain.UserInfo;
import org.hamcrest.Matchers.*;

class UserInfoTest {
    public function new() {
    }

    @Test
    public function format_user_info() {
        assertThat(new UserInfo().formatUserAndGid(), equalTo(null), "format null");
        assertThat(new UserInfo(123).formatUserAndGid(), equalTo("123"), "format uid");
        assertThat(new UserInfo(123, 321).formatUserAndGid(),
                    equalTo("123:321"), "format uid:gid");
        assertThat(new UserInfo(null, 321).formatUserAndGid(),
                    equalTo(null), "format only gid");
    }
}