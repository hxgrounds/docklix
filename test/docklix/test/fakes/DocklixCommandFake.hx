package docklix.test.fakes;

import docklix.DocklixCommand.DocklixOptions;
import org.hamcrest.Matchers;

class DocklixCommandFake extends DocklixCommand{

    var _executedCommandArgs: Array<String>;
    var _executeCommandOptions: DocklixOptions;

    public function new() {
        super(null, null);
    }

    public override function execute(args: Array<String>, ?options: DocklixOptions) {
        _executedCommandArgs = args;
        _executeCommandOptions = options;
    }

    public function verifyLastCommandExecutedWith(args: Array<String>) {
        Matchers.assertThat(_executedCommandArgs, Matchers.notNullValue(Array),
            "No command was executed"
        );
        Matchers.assertThat(_executedCommandArgs, Matchers.equalTo(args),
            "Last command doesn't have the expected arguments"
        );
    }

    public function verifyNoCommandExecuted() {
        Matchers.assertThat(_executedCommandArgs, Matchers.nullValue(),
            "No docklix command should have been executed"
        );
    }

    public function lastCommandOptions() {
        return _executeCommandOptions;
    }
}