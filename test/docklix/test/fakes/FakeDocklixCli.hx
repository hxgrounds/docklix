package docklix.test.fakes;

import org.hamcrest.Matchers;
import massive.munit.Assert;
import tink.cli.Rest;

class FakeDocklixCli extends DocklixCli{
    var docklixLastArgs:Array<String> = null;
    var executedHelp:Bool = false;


    public function new() {
        super(null);
    }

    override function docklix(args:Rest<String>) {
        docklixLastArgs = args;
    }

    override function showHelp() {
        executedHelp = true;
    }

    public function lixArgsShouldBe(expected:Array<String>) {
        Matchers.assertThat(this.lixArgs, Matchers.equalTo(expected),
            "Lix args does not match expected value");
    }
}