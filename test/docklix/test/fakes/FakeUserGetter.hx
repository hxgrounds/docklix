package docklix.test.fakes;

import docklix.domain.UserInfo;

class FakeUserGetter implements UserGetter{
    public var uid(default, default):Null<Int> = null;
    public var gid(default, default):Null<Int> = null;
    public var fetchCallsCount(default, null):Int = 0;


    public function new() {}

    public function fetchUserInfo():Null<UserInfo> {
        fetchCallsCount++;

        return {
            uid: uid,
            gid: gid
        };
    }
}