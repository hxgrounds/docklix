package docklix.test.sys;

import docklix.sys.SystemUserGetter;
import proxsys.FakeSystem;

import org.hamcrest.Matchers.*;


class SystemUserGetterTest {
    var getter:UserGetter;
    var fakeSys:FakeSystem;

    @Before
    public function setup() {
        fakeSys = new FakeSystem();
        getter = new SystemUserGetter(fakeSys);
    }

    @Test
    public function fetch_uid_and_gid() {
        //Given
        var uid = 1001, gid = 1002;
        fakeSys.givenProcess('id', ['-u']).output('$uid');
        fakeSys.givenProcess('id', ['-g']).output('$gid');

        //When
        var userInfo = getter.fetchUserInfo();

        //Then
        assertThat(userInfo, is(notNullValue()), 'userInfo should not be null');
        assertThat(userInfo.uid, is(equalTo(uid)), 'uid should match expected');
        assertThat(userInfo.gid, is(equalTo(gid)), 'gid should match expected');
    }
}